﻿var connection = {};

document.getElementById("resetVerboseButton").addEventListener("click", function (event) {
    document.getElementById("verbose").innerHTML = "";
});

var refli = null;
var refli1 = null;
var refli2 = null;

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var connection = new signalR.HubConnectionBuilder().withUrl("/UserNotificationHub?userId=" + user).build();

    //Disable send button until connection is established
    document.getElementById("sendButton").disabled = true;

    connection.on("TaskProgress", function (user, notificationJson) {
        var encodedMsg = JSON.parse(notificationJson.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;"));

        if (!encodedMsg.hasOwnProperty("Body")) {
            var li = document.createElement("li");
            li.textContent = encodedMsg;
            document.getElementById("messagesList").insertBefore(li, refli1);
            refli1 = li;
            return;
        }

        if (encodedMsg.Body.indexOf('unzip-progress-percent') < 0 && encodedMsg.Body.indexOf('pull-progress-Update') < 0) {
            var li1 = document.createElement("li");
            li1.textContent = encodedMsg.Header + ": " + encodedMsg.Body;
            document.getElementById("messagesList").insertBefore(li1, refli1);
            refli1 = li1;
        } else {
            if (encodedMsg.Body.indexOf('unzip-progress-percent') >= 0)
                document.getElementById("unzipProgress").innerHTML = "<strong>unzip-progress:</strong>" + encodedMsg.Body;
            if (encodedMsg.Body.indexOf('pull-progress-Update') >= 0)
                document.getElementById("pullProgress").innerHTML = "<strong>pull-progress:</strong>" + encodedMsg.Body;
        }
        var li2 = document.createElement("li");
        li2.textContent = encodedMsg.Header + ": " + encodedMsg.Body;
        document.getElementById("verbose").insertBefore(li2, refli2);
        refli2 = li2;
    });

    connection.start().then(function () {
        document.getElementById("sendButton").disabled = false;
        document.getElementById("socketConnection").innerHTML = "Web Socket Connected";
    }).catch(function (err) {
        return console.error(err.toString());
    });

    event.preventDefault();
});